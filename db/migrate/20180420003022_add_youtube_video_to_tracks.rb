class AddYoutubeVideoToTracks < ActiveRecord::Migration[5.1]
  def change
    add_column :tracks, :youtube_video_url, :string
  end
end
