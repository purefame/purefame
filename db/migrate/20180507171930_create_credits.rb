class CreateCredits < ActiveRecord::Migration[5.1]
  def change
    create_table :credits do |t|
      t.string :title
      t.string :url
      t.integer :profile_id
      t.integer :hits_out
      t.integer :hits_in

      t.timestamps
    end
  end
end
