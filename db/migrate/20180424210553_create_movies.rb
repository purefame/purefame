class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :title
      t.text :plot
      t.integer :profile_id
      t.integer :runtime_minutes
      t.datetime :release_date
      t.string :mpaa_rating
      t.string :genre_category
      t.string :trailer_youtube_url
      t.string :budget

      t.timestamps
    end
  end
end
