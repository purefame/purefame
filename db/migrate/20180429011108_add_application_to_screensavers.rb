class AddApplicationToScreensavers < ActiveRecord::Migration[5.1]
  def self.up
    change_table :screensavers do |t|
      t.attachment :application
    end
  end

  def self.down
    remove_attachment :screensavers, :application
  end
end

