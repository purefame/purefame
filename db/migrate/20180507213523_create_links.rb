class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.string :title
      t.integer :profile_id
      t.string :url

      t.timestamps
    end
  end
end
