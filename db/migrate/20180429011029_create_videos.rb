class CreateVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :videos do |t|
      t.string :title
      t.integer :profile_id
      t.string :youtube_video_url

      t.timestamps
    end
  end
end
