class CreateEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :entries do |t|
      t.text :body
      t.integer :profile_id
      t.integer :user_id

      t.timestamps
    end
  end
end
