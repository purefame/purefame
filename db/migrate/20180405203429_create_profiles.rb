class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :name
      t.text :biography
      t.string :top_100
      t.string :guestbook
      t.string :forum
      t.string :contact

      t.timestamps
    end
  end
end
