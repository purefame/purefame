class CreateAlbums < ActiveRecord::Migration[5.1]
  def change
    create_table :albums do |t|
      t.string :title
      t.text :description
      t.string :label
      t.datetime :release_date
      t.integer :profile_id

      t.timestamps
    end
  end
end
