class CreateScreensavers < ActiveRecord::Migration[5.1]
  def change
    create_table :screensavers do |t|
      t.string :title
      t.integer :profile_id
      t.text :description

      t.timestamps
    end
  end
end
