class AddFilmographyToProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :filmography, :text
  end
end