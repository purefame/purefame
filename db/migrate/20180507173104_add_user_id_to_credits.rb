class AddUserIdToCredits < ActiveRecord::Migration[5.1]
  def change
	add_column :credits, :user_id, :integer
  end
end
