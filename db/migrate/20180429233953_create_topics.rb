class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.string :label
      t.integer :profile_id
      t.text :message
      t.integer :user_id

      t.timestamps
    end
  end
end
