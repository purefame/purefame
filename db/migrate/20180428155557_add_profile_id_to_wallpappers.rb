class AddProfileIdToWallpappers < ActiveRecord::Migration[5.1]
  def change
	add_column :wallpapers, :profile_id, :integer
  end
end
