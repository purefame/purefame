module TopicsHelper
	def format_last_post(item)
		html = ''

		if item.class == Topic
			html += 'by ' + item.user.name
			html += ' ' + link_to('view', profile_topic_path(@profile, item))
		else
			html += 'by ' + item.user.name
			html += ' ' + link_to('view', profile_topic_path(@profile, item.topic))
		end 

		html
	end
end
