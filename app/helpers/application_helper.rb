module ApplicationHelper
	def youtube_video_html(width = 560, url = 'https://www.youtube.com/watch?v=9WbCfHutDSE')
		ratio = 315.0 / 560.0
		url = url

		width = width
		height = (ratio * width).round

		umatch = url.match(/^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/(.+)$/)

		return "" if umatch.nil?
		return "" if umatch.length < 3
		
		youtube_video_key = umatch[3].gsub('watch?v=', '')

		youtube_video_url = 'https://www.youtube.com/embed/'
		youtube_video_url += youtube_video_key.nil? ? '9WbCfHutDSE' : youtube_video_key

		'<iframe width="' + width.to_s + '" height="' + height.to_s + '" src="' + youtube_video_url + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
	end
end
