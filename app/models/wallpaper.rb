class Wallpaper < ApplicationRecord
	belongs_to :profile
	has_attached_file :image, 
		styles: {
			wallpaper_large: "1920x1080>",
			wallpaper_medium: "1280x1024>",
			wallpaper_small: "1024x768>",
			large: "750x750>", 
			medium: "450x450>", 
			small: "250x250>", 
			thumb: "100x100>", 
			banner: "375x150#", 
			icon: "75x75#" }, 
		default_url: "/images/:style/missing.png"
  	validates_attachment :image, 
  		:presence => true, 
  		:content_type => { :content_type => /\Aimage\/.*\z/ }
end
