# https://github.com/galetahub/ckeditor

class Profile < ApplicationRecord
	has_many :oldest_photos, -> { order('created_at asc') }, class_name: 'Photo'
	has_many :photos, -> { order('created_at desc') }
	has_many :albums, -> { order('release_date desc') }
	has_many :movies, -> { order('release_date desc') }
	has_many :wallpapers, -> { order('created_at desc') }
	has_many :screensavers, -> { order('created_at desc') }
	has_many :videos, -> { order('created_at desc') }
	has_many :topics, -> { order('created_at desc') }
	has_many :entries, -> { order('created_at desc') }
	has_many :credits, -> { order('hits_in desc') }
	has_many :links, -> { order('created_at desc') }

  def to_param
    "#{id}-#{name.parameterize}"
  end

  def photo_page(page_result, search_for_photo_id)
  	ids = page_result.pluck(:id)
  	
  	if idx = ids.index(search_for_photo_id)
  		return idx + 1
  	end

  end

  def self.find_by_subdomain(subdomain)
  	if subdomain == nil
  		return nil
  	else
  		profiles = self.all
  		names = profiles.pluck(:name)
  		names_parameterized = names.collect {|name| name.parameterize } 
  		if profile_idx = names_parameterized.index(subdomain)
  			return profiles[profile_idx]
  		else
  			return nil
  		end

  	end
  end

end
