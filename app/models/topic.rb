class Topic < ApplicationRecord
	belongs_to :profile
	belongs_to :user
	has_many :posts

	def post_total
		total = 1
		total += posts.count
	end

	def last_post
		return self if posts.count <= 0
		return posts.last
	end
end
