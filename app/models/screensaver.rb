class Screensaver < ApplicationRecord
	belongs_to :profile
	has_attached_file :application
	validates_attachment_file_name :application, :matches => [/exe\Z/]
end
