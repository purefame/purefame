class Album < ApplicationRecord
	belongs_to :profile
	has_many :tracks
	
	has_attached_file :image, 
		styles: { 
			big: "1500x1500>", 
			large: "750x750>", 
			medium: "450x450>", 
			small: "250x250>", 
			thumb: "100x100>", 
			banner: "375x150#", 
			icon: "75x75#" }, 
		default_url: "/images/:style/missing.png"
  	validates_attachment :image, 
  		:presence => true, 
  		:content_type => { :content_type => /\Aimage\/.*\z/ }
end
