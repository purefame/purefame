class Message
	include ActiveModel::Model
	attr_accessor :name, :email, :body, :profile_url
	validates :name, :email, :body, :profile_url, presence: true
end
