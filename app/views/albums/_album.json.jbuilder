json.extract! album, :id, :title, :description, :label, :release_date, :profile_id, :created_at, :updated_at
json.url profile_album_url(@profile, album, format: :json)
