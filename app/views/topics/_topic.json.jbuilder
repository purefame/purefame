json.extract! topic, :id, :label, :profile_id, :message, :user_id, :created_at, :updated_at
json.url topic_url(topic, format: :json)
