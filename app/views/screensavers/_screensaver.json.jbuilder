json.extract! screensaver, :id, :title, :profile_id, :description, :created_at, :updated_at
json.url screensaver_url(screensaver, format: :json)
