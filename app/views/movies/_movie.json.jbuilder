json.extract! movie, :id, :title, :plot, :profile_id, :runtime_minutes, :release_date, :mpaa_rating, :genre_category, :trailer_youtube_url, :budget, :created_at, :updated_at
json.url movie_url(movie, format: :json)
