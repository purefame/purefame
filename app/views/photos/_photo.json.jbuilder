json.extract! photo, :id, :title, :description, :proofile_id, :created_at, :updated_at
json.url image_url(photo, format: :json)
