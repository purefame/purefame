json.extract! credit, :id, :title, :url, :profile_id, :hits_out, :hits_in, :created_at, :updated_at
json.url credit_url(credit, format: :json)
