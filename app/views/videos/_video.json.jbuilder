json.extract! video, :id, :title, :profile_id, :youtube_video_url, :created_at, :updated_at
json.url video_url(video, format: :json)
