json.extract! entry, :id, :body, :profile_id, :user_id, :created_at, :updated_at
json.url entry_url(entry, format: :json)
