json.extract! news, :id, :topic, :title, :body, :created_at, :updated_at
json.url news_url(news, format: :json)
