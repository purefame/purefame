json.extract! profile, :id, :name, :biography, :top_100, :guestbook, :forum, :contact, :created_at, :updated_at
json.url profile_url(profile, format: :json)
