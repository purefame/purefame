class ScreensaversController < ApplicationController
  before_action :set_screensaver, only: [:show, :edit, :update, :destroy]
  before_action :set_profile, except: [:create]

  # GET /screensavers
  # GET /screensavers.json
  def index
    @screensavers = Profile.find(params[:profile_id]).screensavers.paginate(:page => params[:page], :per_page => 50)
  end

  # GET /screensavers/1
  # GET /screensavers/1.json
  def show
  end

  # GET /screensavers/new
  def new
    @screensaver = Screensaver.new
    @screensaver.profile = @profile if !@profile.nil?
  end

  # GET /screensavers/1/edit
  def edit
  end

  # POST /screensavers
  # POST /screensavers.json
  def create
    @screensaver = Screensaver.new(screensaver_params)

    respond_to do |format|
      if @screensaver.save
        format.html { redirect_to @screensaver, notice: 'Screensaver was successfully created.' }
        format.json { render :show, status: :created, location: @screensaver }
      else
        format.html { render :new }
        format.json { render json: @screensaver.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /screensavers/1
  # PATCH/PUT /screensavers/1.json
  def update
    respond_to do |format|
      if @screensaver.update(screensaver_params)
        format.html { redirect_to @screensaver, notice: 'Screensaver was successfully updated.' }
        format.json { render :show, status: :ok, location: @screensaver }
      else
        format.html { render :edit }
        format.json { render json: @screensaver.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /screensavers/1
  # DELETE /screensavers/1.json
  def destroy
    @screensaver.destroy
    respond_to do |format|
      format.html { redirect_to profile_screensavers_url(@profile), notice: 'Screensaver was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_screensaver
      @screensaver = Screensaver.find(params[:id])
    end

    def set_profile
      @profile = !@screensaver.nil? ? @screensaver.profile : Profile.find(params[:profile_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def screensaver_params
      params.require(:screensaver).permit(:title, :profile_id, :description, :application)
    end
end
