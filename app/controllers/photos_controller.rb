class PhotosController < ApplicationController
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :create, :new]
  before_action :set_photo, only: [:show, :edit, :update, :destroy]
  before_action :set_profile, except: [:create, :submit, :accept]

  # GET /photos?page=3
  # GET /photos.json?page=3
  def index
    @photos = Profile.find(params[:profile_id]).oldest_photos.paginate(:page => params[:page], :per_page => 1)
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
  end

  # GET /photos/new
  def new
    @photo = Photo.new
    @photo.profile = @profile if !@profile.nil?
  end

  # GET /photos/1/edit
  def edit
  end

  def submit
    @photo = Photo.new
    @photo.remote_image_url = URI.decode(params[:imageUrl])
    @profiles = Profile.pluck(:name, :id)
  end

  def accept
    @photo = Photo.new(photo_params)
    @photo.title = @photo.profile.name if @photo.title.nil? || @photo.title.empty?
    @profile = @photo.profile
    # Fetch the image and put it into the photo object
    @photo.image = open(URI.parse(photo_params[:remote_image_url]))

    respond_to do |format|
      if @photo.save
        @photos_page = @profile.oldest_photos.paginate(:page => nil, :per_page => 1000)
        page = @profile.photo_page(@photos_page, @photo.id)

        format.html { redirect_to profile_photos_url(@profile, page: page, subdomain: @photo.profile.name.parameterize, host: Settings.host), notice: 'Photo was successfully created.' }
        format.json { render :show, status: :created, location: @photo }
      else
        format.html { render :new }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /photos
  # POST /photos.json
  def create
    @photo = Photo.new(photo_params)

    if photo_params.include?(:remote_image_url)
      # Fetch the image and put it into the photo object
      @photo.image = URI.parse(photo_params[:remote_image_url])
    end

    respond_to do |format|
      if @photo.save
        format.html { redirect_to @photo, notice: 'Photo was successfully created.' }
        format.json { render :show, status: :created, location: @photo }
      else
        format.html { render :new }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photos/1
  # PATCH/PUT /photos/1.json
  def update
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { render :show, status: :ok, location: @photo }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to photos_url, notice: 'Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @photo = Photo.find(params[:id])
    end

    def set_profile
      @profile = !@photo.nil? ? @photo.profile : Profile.find(params[:profile_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_params
      params.require(:photo).permit(:title, :description, :profile_id, :image, :remote_image_url)
    end
end
