class ApplicationController < ActionController::Base
 	protect_from_forgery with: :exception

	before_action :configure_permitted_parameters, if: :devise_controller?

	protected

	def configure_permitted_parameters
   		devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email])
    	devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email])
 	end

    def after_sign_out_path_for(resource_or_scope)
  		root_url(subdomain: nil, host: Settings.host)
  		#URI.parse(request.referer).path if request.referer
	end

	def after_sign_up_path_for(resource)
    	root_url(subdomain: nil, host: Settings.host)
  	end

  	def after_sign_in_path_for(resource)
    	root_url(subdomain: nil, host: Settings.host)
  	end
end