class MessagesController < ApplicationController
	before_action :set_profile, except: [:create]

	def new
		@message = Message.new
		@message.profile_url = profile_url(@profile)
	end

	def create
		@message = Message.new(message_params)

		if @message.valid?
			MessageMailer.contact_me(@message).deliver_now
			redirect_to new_profile_message_url, notice: "Message has been successfully sent."
		else
			render :new
		end
	end


	private


    def set_profile
      @profile = Profile.find(params[:profile_id])
    end

	def message_params
		params.require(:message).permit(:name, :email, :body, :profile_url)
	end
end
