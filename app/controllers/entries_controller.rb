class EntriesController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :create, :new]
  before_action :set_entry, only: [:show, :edit, :update, :destroy]
  before_action :set_profile, except: [:create]
  before_action :verify_owner!, only: [:edit, :update, :destroy]

  # GET /entries
  # GET /entries.json
  def index
    @entries = Profile.find(params[:profile_id]).entries.paginate(:page => params[:page], :per_page => 50)
  end

  # GET /entries/1
  # GET /entries/1.json
  def show
  end

  # GET /entries/new
  def new
    @entry = Entry.new
    @entry.profile = @profile if !@profile.nil?
    @entry.user = User.find(current_user["id"]) if current_user
  end

  # GET /entries/1/edit
  def edit
  end

  # POST /entries
  # POST /entries.json
  def create
    @entry = Entry.new(entry_params)
    @entry.user = User.find(current_user["id"]) if current_user

    respond_to do |format|
      if @entry.save
        format.html { redirect_to @entry, notice: 'Entry was successfully created.' }
        format.json { render :show, status: :created, location: @entry }
      else
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    respond_to do |format|
      if @entry.update(entry_params)
        format.html { redirect_to profile_entry_path(@entry.profile, @entry), notice: 'Entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry }
      else
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    respond_to do |format|
      format.html { redirect_to entries_url, notice: 'Entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

     def set_profile
      @profile = !@entry.nil? ? @entry.profile : Profile.find(params[:profile_id])
    end

    def verify_owner!
      if @entry.user.id != current_user["id"]
        flash[:notice] = "You are not the owner of this entry"
        redirect_to profile_entries_path(@profile)
      end 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(:body, :profile_id, :user_id)
    end
end
