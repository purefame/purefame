class CreditsController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :create, :new]
  before_action :set_credit, only: [:show, :edit, :update, :destroy]
  before_action :set_profile, except: [:create]
  before_action :verify_owner!, only: [:edit, :update, :destroy]
  before_action :verify_only_credit!, only: [:new, :create]

  # profile_credit_in_path GET    /profiles/:profile_id/credits/:credit_id/in
  def in
    @credit = Credit.find(params[:credit_id])
    @credit.hits_in = (@credit.hits_in.nil? ? 0 : @credit.hits_in) + 1
    @credit.save

    redirect_to profile_credits_path(@profile)
  end

  # profile_credit_out_path GET    /profiles/:profile_id/credits/:credit_id/out
  def out
    @credit = Credit.find(params[:credit_id])
    @credit.hits_out = (@credit.hits_out.nil? ? 0 : @credit.hits_out) + 1
    @credit.save

    redirect_to @credit.url
  end


  # GET /credits
  # GET /credits.json
  def index
    @credits = Profile.find(params[:profile_id]).credits.paginate(:page => params[:page], :per_page => 50)

  end

  # GET /credits/1
  # GET /credits/1.json
  def show
  end

  # GET /credits/new
  def new
    @credit = Credit.new
    @credit.profile = @profile if !@profile.nil?
  end

  # GET /credits/1/edit
  def edit
  end

  # POST /credits
  # POST /credits.json
  def create
    @credit = Credit.new(credit_params)
    @credit.user = User.find(current_user["id"]) if current_user

    respond_to do |format|
      if @credit.save
        format.html { redirect_to @credit, notice: 'Credit was successfully created.' }
        format.json { render :show, status: :created, location: @credit }
      else
        format.html { render :new }
        format.json { render json: @credit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /credits/1
  # PATCH/PUT /credits/1.json
  def update
    respond_to do |format|
      if @credit.update(credit_params)
        format.html { redirect_to profile_credit_path(@credit.profile, @credit), notice: 'Credit was successfully updated.' }
        format.json { render :show, status: :ok, location: @credit }
      else
        format.html { render :edit }
        format.json { render json: @credit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /credits/1
  # DELETE /credits/1.json
  def destroy
    @credit.destroy
    respond_to do |format|
      format.html { redirect_to profile_credits_url(@profile), notice: 'Credit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit
      @credit = Credit.find(params[:id])
    end

     def set_profile
      @profile = !@credit.nil? ? @credit.profile : Profile.find(params[:profile_id])
    end

    def verify_owner!
      if @credit.user.id != current_user["id"]
        flash[:notice] = "You are not the owner of this website credit"
        redirect_to profile_credits_path(@profile)
      end 
    end

    # new && create
    def verify_only_credit!
      profile_id = @profile.nil? ? params[:credit][:profile_id] : @profile.id
      if current_user && Credit.where(profile_id: profile_id, user_id: current_user["id"]).any?
        flash[:notice] = "You already have a website credit to promote"
        redirect_to profile_credits_path(@profile)
      end 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_params
      params.require(:credit).permit(:title, :url, :profile_id, :user_id)
    end
end
