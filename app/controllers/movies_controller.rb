class MoviesController < ApplicationController
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :create, :new]
  before_action :set_movie, only: [:show, :edit, :update, :destroy]
  before_action :set_profile, except: [:create]

  # GET /movies
  # GET /movies.json
  def index
    @movies = Profile.find(params[:profile_id]).movies.paginate(:page => params[:page], :per_page => 50)
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
  end

  # GET /movies/new
  def new
    @movie = Movie.new
    @movie.profile = @profile if !@profile.nil?
  end

  # GET /movies/1/edit
  def edit
  end

  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to profile_movies_url(@profile), notice: 'Movie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    def set_profile
      @profile = !@movie.nil? ? @movie.profile : Profile.find(params[:profile_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.require(:movie).permit(:title, :plot, :profile_id, :image, :runtime_minutes, :release_date, :mpaa_rating, :genre_category, :trailer_youtube_url, :budget)
    end
end
