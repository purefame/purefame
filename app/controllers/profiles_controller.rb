class ProfilesController < ApplicationController
  #before_action :find_profile
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :create, :new]
  before_action :set_profile, only: [:show, :media, :biography, :filmography, :edit, :update, :destroy]

  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    @photos = @profile.photos
    @photos_page = @profile.oldest_photos.paginate(:page => nil, :per_page => 1000)
  end

  # GET /profiles/1/biography
  # GET /profiles/1/biography.json
  def biography
  end

  # GET /profiles/1/filmography
  # GET /profiles/1/filmography.json
  def filmography
  end

  # GET /profiles/1/media
  # GET /profiles/1/media.json
  def media
    @wallpapers = Profile.find(params[:profile_id]).wallpapers
    @screensavers = Profile.find(params[:profile_id]).screensavers
    @videos = Profile.find(params[:profile_id]).videos
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
=begin
  def find_profile
    #logger.info ''
    case request.host
    when "www.#{Settings.host}", Settings.host, nil
    else     
      if request.host.index(Settings.host)
        rhsf = request.host.split('.').first
     #   logger.info '>> request.host[' + request.host + '].index(Settings.host) ' + Settings.host + ', rhsf: ' + rhsf

        @current_profile = Profile.find_by_subdomain(rhsf)

      end

      if !@current_profile
        #logger.info 'redirect_to Settings.domain (' + Settings.domain + ')'
        # redirect_to Settings.domain
      end

    end  
  end
=end

    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      # @blog = Blog.find_by_subdomain!(request.subdomain)
      #logger.info '>> @current_profile.inspect::' + @current_profile.inspect

      if @current_profile
        @profile = @current_profile
      else
        id = !params[:profile_id].nil? ? params[:profile_id] : params[:id]
      
        @profile = Profile.find(id)
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.require(:profile).permit(:name, :biography, :filmography)
    end
end
