class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]
  before_action :set_profile, except: [:create]
  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy]
  before_action :verify_owner!, only: [:edit, :update, :destroy]

  # GET /topics
  # GET /topics.json
  def index
    @topics = @profile.topics
  end

  # GET /topics/1
  # GET /topics/1.json
  def show
  end

  # GET /topics/new
  def new
    @topic = Topic.new
    @topic.profile_id = @profile.id if !@profile.nil?
    @topic.user = User.find(current_user["id"]) if current_user
  end

  # GET /topics/1/edit
  def edit
  end

  # POST /topics
  # POST /topics.json
  def create
    @topic = Topic.new(topic_params)
    @topic.user = User.find(current_user["id"]) if current_user
   
    respond_to do |format|
      if @topic.save
        format.html { redirect_to @topic, notice: 'Topic was successfully created.' }
        format.json { render :show, status: :created, location: @topic }
      else
        format.html { render :new }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topics/1
  # PATCH/PUT /topics/1.json
  def update
    respond_to do |format|
      if @topic.update(topic_params)
        format.html { redirect_to @topic, notice: 'Topic was successfully updated.' }
        format.json { render :show, status: :ok, location: @topic }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    @topic.destroy
    respond_to do |format|
      format.html { redirect_to topics_url, notice: 'Topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    def set_profile
      @profile = !@topic.nil? ? @topic.profile : Profile.find(params[:profile_id])
    end

    def verify_owner!
      puts "[[[]]] verify_owner! #{@topic.user.id} == #{current_user["id"]}"

      if @topic.user.id != current_user["id"]
        flash[:notice] = "User is not the owner of this topic"
        redirect_to root_path
      end 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:label, :profile_id, :message)
    end
end
