class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :create, :new]
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :set_topic, except: [:create]
  before_action :set_profile, except: [:create]
  before_action :verify_owner!, only: [:edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
    redirect_to profile_topic_path(@profile, @topic)  
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
    @post.topic = @topic if !@topic.nil?
    @post.user = User.find(current_user["id"]) if current_user
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user = User.find(current_user["id"]) if current_user

    respond_to do |format|
      if @post.save
        format.html { redirect_to profile_topic_posts_path(@post.topic.profile, @post.topic), notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to profile_topic_posts_path(@post.topic.profile, @post.topic), notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    def set_profile
      @profile = !@topic.nil? ? @topic.profile : Profile.find(params[:profile_id])
    end

    def set_topic
      @topic = !@post.nil? ? @post.topic : Topic.find(params[:topic_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:body, :topic_id)
    end

    def verify_owner!
      puts "[[[]]] verify_owner! #{@topic.user.id} == #{current_user["id"]}"

      if @post.user.id != current_user["id"]
        flash[:notice] = "You are not the owner of this post"
        redirect_to profile_topic_path(@profile, @topic)
      end 
    end
end
