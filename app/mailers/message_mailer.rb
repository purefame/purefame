class MessageMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.message_mailer.contact_me.subject
  #

	def contact_me(message)
		@body = message.body
		@profile_url = message.profile_url
		@email = message.email
		mail to: "cavazzi@purefame.com", from: "cavazzi@purefame.com"
	end
end
