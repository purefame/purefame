require 'test_helper'

class ScreensaversControllerTest < ActionDispatch::IntegrationTest
  setup do
    @screensaver = screensavers(:one)
  end

  test "should get index" do
    get screensavers_url
    assert_response :success
  end

  test "should get new" do
    get new_screensaver_url
    assert_response :success
  end

  test "should create screensaver" do
    assert_difference('Screensaver.count') do
      post screensavers_url, params: { screensaver: { description: @screensaver.description, profile_id: @screensaver.profile_id, title: @screensaver.title } }
    end

    assert_redirected_to screensaver_url(Screensaver.last)
  end

  test "should show screensaver" do
    get screensaver_url(@screensaver)
    assert_response :success
  end

  test "should get edit" do
    get edit_screensaver_url(@screensaver)
    assert_response :success
  end

  test "should update screensaver" do
    patch screensaver_url(@screensaver), params: { screensaver: { description: @screensaver.description, profile_id: @screensaver.profile_id, title: @screensaver.title } }
    assert_redirected_to screensaver_url(@screensaver)
  end

  test "should destroy screensaver" do
    assert_difference('Screensaver.count', -1) do
      delete screensaver_url(@screensaver)
    end

    assert_redirected_to screensavers_url
  end
end
