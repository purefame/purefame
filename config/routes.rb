require 'subdomain'

Rails.application.routes.draw do
	get '/submit-image-to-profile' => 'photos#submit'
	post '/accept-image-to-profile' => 'photos#accept'

	resources :news
	resources :links
	resources :credits
	resources :entries
	resources :posts
	resources :topics
	resources :videos
	resources :screensavers
	resources :wallpapers

	devise_for :users, controllers: { 
		sessions: 'users/sessions', 
		registrations: 'users/registrations', 
		passwords: 'users/passwords' 
	}
	devise_for :admins, controllers: { 
		sessions: 'admins/sessions', 
		registrations: 'admins/registrations', 
		passwords: 'admins/passwords' 
	}

	resources :movies
	resources :tracks
	resources :photos
	resources :albums
#	resources :profiles
	
	# match '', to: 'blogs#show', constraints: lambda { |r| r.subdomain.present? && r.subdomain != 'www' }
	
#	constraints(Subdomain) do
#	    get '/' => 'profiles#show'
	    
	   
	    #resources :posts do
	    #  collection do
	    #    get :search
	    #  end
	    #end

		resources :profiles do
			resources :messages
	#		post 'contact', to: 'messages#create', as: 'create_message'
	#		get 'contact', to: 'messages#new', as: 'new_message'
		  	resources :movies
			resources :photos
			resources :albums do 
				resources :tracks
			end
			get 'filmography'
			get 'biography'
			get 'media'
			resources :wallpapers
			resources :screensavers
			resources :videos
			resources :topics do
				resources :posts
			end
			resources :entries
	 		resources :credits do
	 			get 'in'
	 			get 'out'
	 		end
			resources :links
			resources :news
		end

#	  end 

	# resources :profiles, constraints: lambda { |r| r.subdomain.present? && r.subdomain != 'www' } do
	
	get 'home/index' #, :defaults => { :format => 'html' }
	get 'home/order' #, :defaults => { :format => 'html' }

	root 'profiles#index' #, :defaults => { :format => 'html' }

  	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
