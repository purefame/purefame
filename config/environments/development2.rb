require 'socket'
require 'ipaddr'

Rails.application.configure do
  config.web_console.whitelisted_ips = Socket.ip_address_list.reduce([]) do |res, addrinfo|
    addrinfo.ipv4? ? res << IPAddr.new(addrinfo.ip_address).mask(24) : res
  end

  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = true

  # Do not eager load code on boot.
  config.eager_load = true

  # Show full error reports.
  config.consider_all_requests_local = false

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    # config.cache_store = :memory_store
    # config.public_file_server.headers = {
    #  'Cache-Control' => "public, max-age=#{2.days.seconds.to_i}"
    #}
    config.cache_store = :dalli_store
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  config.action_dispatch.tld_length = 2

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = false

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  
  Paperclip.options[:command_path] = "/usr/local/bin/"

  config.action_mailer.default_url_options = { host: 'local.purefame.com' }

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.smtp_settings = {
   :address               => 'smtp.zoho.com',
#   :port                  => 587,
   :port                  => 465,
   :domain                => 'zoho.com',
   :user_name             => 'cavazzi@purefame.com',
   :password              => 'KNBC1011AXi',
#   :authentication        => :plain,
   :authentication        => :login,
  :ssl                    => true,
  :tls                    => false,
  :enable_starttls_auto   => true 
 }
end
