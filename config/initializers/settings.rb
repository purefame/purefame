# Use this setup block to configure all options available in Settings.
module Settings
  # Configure to the primary domain name of the application
  @@host = ActionMailer::Base.default_url_options[:host]
  @@domain = ActionMailer::Base.default_url_options[:host]

    def self.host
      return @@host
    end

    def self.domain
      return @@domain
    end
end
